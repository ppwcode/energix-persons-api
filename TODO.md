# TODOs

## MUDO

-   use our private `npm` registry (do this together with Maarten)
    -   setup `.npmrc`
    -   make sure BB|\_ also can work with our private registry
-   publish the Joi schemata to our `nmp` registry (confer with Jan)

-   after that, repeat for the `raa/2` service

## IDEA

-   build a 1-HTML-file version of the OpenAPI spec (with openapi cli), and publish that file in a secure weblocation
    -   deploy by BB|\_ on each succesful build, under the build number
    -   only developers should be able to read those files
