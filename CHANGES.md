# Changes

# 1

## 1.0

## 1.0.0

-   Now use common OpenAPI things via an `npm` dependency on `@ppwcode/openapi` (the dependency via a git submodule is
    removed)
-   The Joi schemata are moved from `schemata/…` to `api/…`, so they are next to there counterpart OpenAPI models

# 0

## 0.0

### 0.0.0

Initial release
